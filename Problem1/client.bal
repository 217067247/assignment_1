import  ballerina/http;
import ballerina/io;
import ballerina/lang.'int as ints;

    

    public function main() returns error?{

        final Client cl = check new();

        io:println("Welcome to the Virtual Learning Environment");
        io:print("_______________________________________________");
        boolean end = false;

        while (end == false) {
            io:println("Please select an action to perform: ");
            io:println("_____________________________________");
            io:println("1. Add a Learner");
            io:println("2. Get list of Learners");
            io:println("3. Get a Learner");
            io:println("4. Add a course");
            io:println("5. Close system");

            string choice = io:readln("Enter your Choice: ");
            
            match choice{
                "1" => {
                    Learner[] lner = [];
                    string sid = io:readln("Enter ID: ");
                    string Uname = io:readln("Enter student Username: ");
                    int|error id1 = ints:fromString(sid);
                    int idd = check id1;
                    string Lname = io:readln("Enter student last name: ");
                    string Fname = io:readln("Enter student first name: ");
                    boolean end2 = false;
                    string[] fmt = [];
                    io:println("Enter prefared formats: ");
                    while(end2 == false) {

                        string fmt2 = io:readln("Preferred format: ");
                        fmt.push(fmt2);
                        string dec = io:readln("Add another preferred format? Y/N: ");
                        if (dec == "Y") {
                            end2 = false;
                            
                        }else if(dec == "N"){
                            end2 = true;
                        }else {
                            io:println("invalid selection");
                            end2 = true;
                        }

                        
                    }
                    end2 = false;
                    PastSubject[] j = [];
                    io:println("Enter Past subjects: ");
                    while(end2 == false){
                        string corse = io:readln("enter course name: ");
                        string sco = io:readln("Enter Score: ");

                        PastSubject jl = {course: corse, score: sco};
                        string dec = io:readln("Add another past subject? Y/N: ");
                        j.push(jl);

                        if(dec == "Y"){
                            end2 = false;
                        }else if(dec == "N"){
                            end2 = true;
                        }else {
                            io:println("invalid selection");
                            end2 = true;
                        }



                    }

                    Learner en = {
                        id: check id1,
                        username: Uname,
                        lasname: Lname,
                        firstname: Fname,
                        preferred_formats: fmt,
                        past_subjects: j
                    };


                    error? learner = cl ->addLearner(en);

                    io:println(learner);

                }

                "2" => {
                    Learners|error learner = check cl ->getLearners();
                    io:println(learner);
                }

                "3" => {
                    string tid = io:readln("Enter student id: ");
                    int|error lid = ints:fromString(tid);
                    int hid = check lid;
                    Learner|error l = check cl->getLearner(hid);
                }
                "4" => {

                    

                    string cid = io:readln("enter course code: ");
                    string cname = io:readln("Enter course name: ");
                    boolean end3 = false;
                    Audioinp[] np = [];
                    Text[] txt = [];

                    while(end3 == false){
                        string nm = io:readln("Enter topic name: ");
                        string auddes = io:readln("Enter description: ");
                        string dif = io:readln("Difficulty: ");
                        Audioinp lp = {
                            name: nm,
                            aud_discription: auddes,
                            difficulty: dif
                        };
                        np.push(lp);
                        string dec = io:readln("Add another audio Y/N: ");
                        if(dec == "Y"){
                            end3 = false;
                        }else if(dec == "N"){
                            end3 = true;
                        }else{
                            end3 = true;
                        }


                    }
                    
                    while(end3 == true){
                        string[] tx = [];
                        string tex = io:readln("Add Text: ");
                        tx.push(tex);
                        txt.push(tx);
                        string dec = io:readln("Add another text? Y/N: ");
                        if(dec == "Y"){
                            end3 = true;
                        }else if(dec == "N"){
                            end3 = false;
                        }else{
                            end3 = false;
                        }
                    }

                    string[] vid = [];
                    string[] aud = [];

                    while(end3 == false){
                        string vd = io:readln("add video");
                        vid.push(vd);
                        string dec = io:readln("Add another text? Y/N: ");
                        if(dec == "Y"){
                            end3 = false;
                        }else if(dec == "N"){
                            end3 = true;
                        }else{
                            end3 = true;
                        }

                    }
                    while(end3 == true){
                        string ad = io:readln("add audio");
                        aud.push(ad);
                        string dec = io:readln("Add another text? Y/N: ");
                        if(dec == "Y"){
                            end3 = true;
                        }else if(dec == "N"){
                            end3 = false;
                        }else{
                            end3 = false;
                        }

                    }


                    Course c = {
                        Course_id: cid,
                        course: cname,
                        Learning_objects: {
                            Required: {
                                audio: np
                            },
                            suggested: {
                                video: vid,
                                audio: aud
                            }
                        }
                    };
                    
                    error? crse = cl ->addCourse(c);

                          
                }
                "5"=>{
                    string dec = io:readln("Exit system? Y/N: ");
                    if(dec == "N"){
                            end = false;
                        }else if(dec == "Y"){
                            end = true;
                        }else{
                            end = true;
                        }
                }

            }



            
        }
        
        
    }
# this is an API that handles Learners and subjects
#
# + clientEp - Connector http endpoint
public client class Client {


    http:Client clientEp;
    public isolated function init(http:ClientConfiguration clientConfig =  {}, string serviceUrl = "http://localhost:9090/learnersve") returns error? {
        http:Client httpEp = check new (serviceUrl, clientConfig);
        self.clientEp = httpEp;
    }
    # Get Learners
    #
    # + return - succesful list of Learners
    remote isolated function getLearners() returns Learners|error {
        string  path = string `/learners`;
        Learners response = check self.clientEp-> get(path, targetType = Learners);
        return response;
    }
    # Add Learner
    #
    # + payload - This is the learner object.
    # + return - A Learner has been created
    remote isolated function addLearner(Learner payload) returns error? {
        string  path = string `/learners`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
         _ = check self.clientEp-> post(path, request, targetType=http:Response);
    }
    # Get a Learner
    #
    # + id - the learner id
    # + return - succesful return of a learner
    remote isolated function getLearner(int id) returns Learner|error {
        string  path = string `/learners/${id}`;
        Learner response = check self.clientEp-> get(path, targetType = Learner);
        return response;
    }
    # Update a Learner
    #
    # + id - the learner id
    # + payload - This is the Learner object
    # + return - Learner updated
    remote isolated function updateLearner(int id, Learner payload) returns error? {
        string  path = string `/learners/${id}`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
         _ = check self.clientEp-> put(path, request, targetType=http:Response);
    }
    # add a course
    #
    # + payload - this is the course object
    # + return - A course has been added
    remote isolated function addCourse(Course payload) returns error? {
        string  path = string `/course`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
         _ = check self.clientEp-> post(path, request, targetType=http:Response);
    }
}
