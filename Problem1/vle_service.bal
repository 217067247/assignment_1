import  ballerina/http;
import ballerina/io;


Learner[] all_learners = [];
Learners ln = all_learners;
Course[] all_courses = [];

listener  http:Listener  ep0  = new (9090, config  = {host: "localhost"});

 service  /learnersve  on  ep0  {
        resource  function  get  learners()  returns  Learner[] {
            io:println("Handling GET request from /leanersve/learners");
            return ln;
    }
        resource  function  post  learners(@http:Payload  {} Learner  payload)  returns  http:Created {

            io:println("Handling POST request to /learnersve/learners");
            ln.push(payload);
            http:Created created = {body: "new Learner succesfully added"};
            return created;
    }
        resource  function  get  learners/[int  id]()  returns  Learner {
            Learner[] l = [];
            foreach var item in ln {
                if(item.id == id) {
                    l.push(item);
                }
                
            }

            return l[0];
    }
        resource  function  put  learners/[int  id](@http:Payload  {} Learner  payload)  returns  http:Ok {
            foreach var item in ln {
                if(item.id == id) {
                    
                }
                
            }
            http:Ok ok = {body: "Learner profile succesfully updated"};
            return ok;
    }
        resource  function  post  course(@http:Payload  {} Course  payload)  returns  http:Created {
            all_courses.push(payload);
            http:Created created = {body: "new course succesfully added"};
            return created;
    }
}
