public type Audioinp record {
    string name?;
    string aud_discription?;
    string difficulty?;
};

public type Text anydata[];

public type Learners Learner[];

public type Course record {
    string Course_id;
    string course;
    record  { record  { Audioinp[] audio?;}  Required?; record  { anydata[] video?; anydata[] audio?;}  suggested?;}  Learning_objects?;
};

public type PastSubject record {
    string course?;
    string score?;
};

public type Learner record {
    int id;
    string username;
    string lasname?;
    string firstname?;
    anydata[] preferred_formats?;
    PastSubject[] past_subjects?;
};
